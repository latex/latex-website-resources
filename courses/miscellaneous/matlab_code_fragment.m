% Plot of sin(t)
t = linspace(0, 2 * pi, 50);
s = sin(t);
plot(t, s);

% Axis and labels
axis([0, 2 * pi, -1, 1]);
box on;
xlabel('$t$', ...
       'Interpreter', 'LaTeX', ...
       'FontSize', 18);
ylabel('$\sin (t)$', ...
       'Interpreter', ...
       'LaTeX', 'FontSize', 18);