;; This file can be used in the G-Databar at DTU to configure Emacs for writing
;; LaTeX documents.
;;
;; Made by Jacob Bunk Nielsen and Morten Ovi, edited by Martin Jesper Low
;; Madsen.
;;
;; Read more at http://www.latex.dtu.dk/.
;;
;; Lines in this file that begin with `;' are comments. If desired, parts of the
;; configuration can be turned off by adding `;' in front.

;; Starts Emacs as a server which enables inverse search in TeX/LaTeX
(server-start)

;; Colors
(setq default-frame-alist
      '((cursor-color . "black")         ; Black cursor color
        (background-color . "grey")      ; Grey background
        (vertical-scroll-bars . right))) ; Scroll bars on the right

;; Maps the delete key to delete the character to the right of the cursor. This
;; is a fix for the databar
(global-set-key [delete] 'delete-char)

;; Map the home and end keys
(global-set-key [home] 'beginning-of-line)
(global-set-key [end] 'end-of-line)
(global-set-key [C-home] 'beginning-of-buffer)
(global-set-key [C-end] 'end-of-buffer)

;; Force UTF-8 to enable accented letters
(set-language-environment "utf-8")

;; Highlights the matching parenthesis of the one that the cursor is currently
;; at
(show-paren-mode t)

;; Enables syntax highlighting
(global-font-lock-mode t)

;; Turns on line- and column numbers in the buffer statusbar
(setq line-number-mode t)
(setq column-number-mode t)
(line-number-mode t)
(column-number-mode t)

;; Automatically replaces a text selection when new text is typed
(delete-selection-mode t)

;; Enables Windows-like cut, copy and paste behavior
(cua-mode t)

;; Show the selected area. This is turned on by default
(transient-mark-mode t)

;; Activate AUCTeX
(require 'tex-site)

;; Turn on RefTeX when a LaTeX document is opened
(add-hook 'LaTeX-mode-hook 'turn-on-reftex) ; AUCTeX's LaTeX-mode
(add-hook 'latex-mode-hook 'turn-on-reftex) ; Emacs' latex-mode

;; Shows an index of sections, subsections etc. for the current document in the
;; top menu
(setq reftex-load-hook (quote (imenu-add-menubar-index)))
(setq reftex-mode-hook (quote (imenu-add-menubar-index)))

;; Turns on auto-fill-mode for LaTeX documents. It automatically wraps text when
;; a line exceeds 80 columns (see the default-fill-column line below)
(add-hook 'LaTeX-mode-hook 'turn-on-auto-fill) ; AUCTeX's LaTeX-mode
(add-hook 'latex-mode-hook 'turn-on-auto-fill) ; Emacs' latex-mode

;; Turns on inverse search and enables LaTeX-math-mode that allows you to insert
;; mathematical symbols using "`" followed by a certain character (see M-x
;; describe-mode LaTeX-math-mode for a list of these shortcuts)
(add-hook 'LaTeX-mode-hook
	  (lambda ()
	    (TeX-source-specials-mode t)
	    (LaTeX-math-mode t)
	    (run-hooks LaTeX-math-mode)))

;; Default fill column width (wraps text that is longer than 80 columns). Force
;; it with M-q
(setq default-fill-column 80)

;; Makes an alias for `yes' and `no' prompts to `y' and `n'
(defalias 'yes-or-no-p 'y-or-n-p)

;; Configure AUCTeX to use `xdvi' for DVI viewing
(setq tex-dvi-view-command "xdvi")

;; Do what I mean! Compile the active buffer (current open file) and try not to
;; query for a master file
(setq TeX-master 'dwim)
(put 'LaTeX-hide-environment 'disabled nil)

;; Automatically loads flyspell, a spell checking tool
(autoload 'flyspell-mode "flyspell" "On-the-fly spelling checker." t)

;; Disable blinking cursor
(blink-cursor-mode -1)

;; Turn off beep sounds when errors occur
;(setq visible-bell t)

;; Enable mousewheel
(when window-system
  (require 'mwheel)
  (mwheel-install)
  (setq x-select-enable-clipboard t))
